<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test de personalité Surveyroot</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Cinzel&family=Nunito:&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-image: url("/images/background-image10.jpg");
                color: black;
                font-family: 'Cinzel', serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                background-size: cover;
                background-attachment: fixed;
                /*background-position: center;*/
                background-repeat: no-repeat;
                min-height: 1000px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 30px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 50px;
                font-size: 18px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Bonjour, <br>Veuillez-vous connecter ou vous identifier<br>afin d'accéder à nos tests de personalité
                </div>

                @if (Route::has('login'))
                <div class="links">
                    @auth
                        <a href="{{ url('/sondages') }}">Accueil</a>
                    @else
                        <a href="{{ route('login') }}">Se connecter</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">S'enregister</a>
                        @endif
                    @endauth
                </div>
            @endif
            </div>
        </div>
    </body>
</html>
