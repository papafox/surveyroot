@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Controlez votre adresse e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nouveau lien de vérification a été envoyé à votre adresse électronique.') }}
                        </div>
                    @endif

                    {{ __('Avant de poursuivre, veuillez vérifier votre boîte mail avec le lien de vérification.') }}
                    {{ __("Si vous n'avez pas reçu d'e-mail") }}, <a href="{{ route('verification.resend') }}">{{ __('cliquez ici pour en demander une autre') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
