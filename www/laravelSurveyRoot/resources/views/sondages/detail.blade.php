@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card bg-light border secondary pt-5">
        <div class="card-header">
            <h5>{{ $sondage->titre}}</h5>
        </div>
       <div class="card-body">
            
        </div>
        <div class="card-footer text-muted">
            <p>mis en ligne le {{ date_format($recette->created_at, "d F Y") }}</p>
            <div class="d-flex justify-content-around">
                <a href="{{ URL::to('sondages/'.$sondage->id.'/edit') }}">
                    <button type="button" class="btn btn-primary btn-sm">Modifier</button>
                 </a>
                <form action="/sondages/{{ $sondage->id }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE" />
                    <button type="submit" class="btn btn-alert btn-sm">Supprimer</button>
                </form>
            </div>   
            
        </div>
    </div>
</div>
@endsection