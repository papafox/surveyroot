@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-12 d-flex justify-content-center pt-5">
            <h1>Modification du profil</h1>
        </div>
        <div class="offset-2 col-sm-8">
            <form action="/sondages/{{ $profil->id }}/updateProfil" method="post">
                @method('PUT')
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Le formulaire contient des erreurs
                    </div>
                @endif

                @csrf
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea type="text" class="form-control @error('description') is-invalid @enderror" 
                    id="description" name="description" placeholder="description"> {{ old('description', $profil->description_profil) }}</textarea>
                    @error('description')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="ScoreMin">Score minimum</label>
                    <input type="number" class="form-control @error('ScoreMin') is-invalid @enderror" id="ScoreMin" name="ScoreMin" placeholder="Score minimum" value="{{ old('ScoreMin', $profil->score_min) }}">
                    @error('ScoreMin')
                        <span class="text-danger help-block"> {{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="ScoreMax">Score maximum</label>
                    <input type="number" class="form-control @error('ScoreMax') is-invalid @enderror" id="ScoreMax" name="ScoreMax" placeholder="Score maximum" value="{{ old('ScoreMax', $profil->score_max) }}">
                    @error('ScoreMax')
                        <span class="text-danger help-block"> {{ $message }} </span>
                    @enderror
                </div>
	            <button type="submit" class="btn btn-primary btn-sm">Modifier le profil</button>
            </form>
    </div>
</div>

@endsection