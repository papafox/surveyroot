@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-12 d-flex justify-content-center pt-5">
            <h1>Modification du Sondage</h1>
        </div>
        <div class="offset-2 col-sm-8">
            <form action="/sondages/{{ $sondage->id }}" method="post">
                @method('PUT')
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Le formulaire contient des erreurs
                    </div>
                @endif

                @csrf
                <div class="form-group">
                    <label for="titre">Titre</label>
                    <textarea type="text" class="form-control @error('titre') is-invalid @enderror" 
                    id="titre" name="titre" placeholder="titre"> {{ old('titre', $sondage->titre) }}</textarea>
                    @error('titre')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
	            <button type="submit" class="btn btn-primary btn-sm">Modifier le Sondage</button>
            </form>
    </div>
</div>

@endsection