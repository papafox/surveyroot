@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-12 d-flex justify-content-center pt-5">
        <h1>Modification de sondage {{ $data['sondage']->titre }}</h1> 
        <div class="d-flex float-left ml-3 mr-3">
            <a href="{{ URL::to('sondages/'.$data['sondage']->id.'/edit') }}">
                <button type="button" class="btn btn-warning btn-sm">Modifier</button>
             </a>
        </div>
        <div>
            <form class="ml-3" action="/sondages/{{ $data['sondage']->id }}" method="post">
                @csrf
                <input type="hidden" name="_method" value="DELETE"> 
                <button type="submit" class="btn btn-danger btn-sm"> Supprimer </button>
            </form>
        </div>
        </div>
        <div class="offset-2 col-sm-8">
        
            <h2 class="d-flex"> Les Questions </h2>
            <div class="pb-2">
                <a href="{{ URL::to('questions/'.$data['sondage']->id.'/createQ') }}">
                    <button type="button" class="btn btn-primary btn-sm">Créer une question</button>
                </a>
            </div>
            @foreach ($data['sondage']->questions()->get() as $question)
                <div class="list-group-item">
                    <h5>{{ $question->question_text}}</h5>
                    @foreach ($question->reponses()->get() as $reponse)
                        Réponse : {{ $reponse->texte_reponse }} <br/>
                        Score :  {{ $reponse->score_reponse }} <br/>
                    @endforeach
                    <!-- Modifier la question -->
                    <div class="pb-2 d-flex float-left">
                        <a href="{{ URL::to('questions/'.$question->id.'/editQ') }}">
                            <button type="button" class="btn btn-warning btn-sm">Modifier la question</button>
                        </a>
                    </div>
                    <!-- Supprimer la question -->
                    <div class="pb-2 d-flex">
                        <form action="/questions/{{ $question->id }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE" />
                            <button type="submit" class="btn btn-danger btn-sm">Supprimer la question</button>
                        </form>
                    </div>
                </div>  
            @endforeach
        </div>
        <div class="offset-2 col-sm-8">
            <h2> Les Profils </h2>
                <a href="{{ URL::to('sondages/'.$data['sondage']->id.'/createProfil') }}">
                    <button type="button" class="btn btn-primary btn-sm">Créer un profil</button>
                </a>
                <div class="list-group-item">
                    <h4>Informations Profils</h4>
                    <p>Score minimum possible du sondage : {{$data['scoreMin']}}</p>
                    <p>Score maximum possible du sondage : {{$data['scoreMax']}}</p>
                    <p>Score non profilé :
                    @for ($i = 0; $i < count($data['plages']); $i++)
                     {{$data['plages'][$i]}}
                    @endfor
                    </p>
                </div>
            @foreach ($data['sondage']->profils()->get() as $profil)
                <div class="list-group-item">
                    <p>Description : {{  $profil->description_profil}}</p>
                    <p>Score minimum : {{ $profil->score_min }} </p>
                    <p>Score maximum : {{ $profil->score_max }} </p>
                    <a href="{{ URL::to('sondages/'.$profil->id.'/editProfil') }}">
                        <button type="button" class="btn btn-warning btn-sm">Modifier le profil</button>
                    </a>
                    <a href="{{ URL::to('sondages/'.$profil->id.'/DeleteProfil') }}">
                        <button type="button" class="btn btn-danger btn-sm">Supprimer le profil</button>
                    </a>
                </div>  
            @endforeach
        </div>
    </div>
</div>

@endsection