@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">
            
        </div>
        <div class="card-body">
            <!-- afficher le resultat -->
            <p>Votre resultat : {{$data['score']}} / {{$data['scoreMax']}}</p>
            <p>Votre profil : {{$data['profil']->description_profil}}</p>
        </div>
        <div class="card-footer text-muted">

        </div>
    </div>
</div>
@endsection