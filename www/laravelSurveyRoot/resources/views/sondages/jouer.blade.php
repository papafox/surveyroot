@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card">
    <div class="card-header">
        <h5>{{ $sondage->titre }}</h5>
    </div>
    <div class="card-body">
        <!-- affichage des questions -->
        <form action="/resultat" method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="sondage_id" value="{{$sondage->id}}">
            @foreach ($sondage->questions()->get() as $question)
                <div class="list-group-item">
                    <h5>{{ $question->question_text}}</h5>
                    @foreach ($question->reponses()->get() as $reponse)
                    <p>
                        <input type="radio" name="question{{$question->id}}" value="{{$reponse->score_reponse}}" required> 
                        <label for="question{{$question->id}}">{{$reponse->texte_reponse}} </label>
                    </p> 
                    @endforeach
                </div>  
            @endforeach
            <button type="submit">Resultat</button>
        </form>
    </div>
    <div class="card-footer text-muted">
        <p>mis en ligne le {{ date_format($sondage->created_at, "d F Y") }} </p>
    </div>
    </div>
</div>
@endsection