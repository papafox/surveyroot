@extends('layouts.app')

@section('content')


<div class="container">
    <div class="list-group">
        @auth
            @if(Auth::user()->estAdmin())
                <a href="{{ URL::to('sondages/create') }}">
                    <button type="button" class="btn btn-primary btn-sm">Créer un nouveau sondage</button>
                </a>
            @endif
         @endauth
    @foreach ($sondages as $sondage)
        <div class="list-group-item">
            <h5>{{ $sondage->titre}}</h5>
            <p> mis en ligne le {{ date_format($sondage->created_at, "d F Y") }} </p>
            @auth
                @if(!Auth::user()->aDejaJouer($sondage->id))
                    <div class="d-flex float-left">
                        <a href="{{ URL::to('sondages/'. $sondage->id) }}"> 
                            <button type="button" class="btn btn-success btn-sm">Jouer</button>
                        </a>
                    </div>
                @else
                    <div class="d-flex float-left">
                        <a href="{{URL::to(Auth::user()->id.'/'. $sondage->id.'/resultat') }}"> 
                            <button type="button" class="btn btn-warning btn-sm">Resultat</button>
                        </a>
                    </div>
                @endif
                <div class="d-flex float-left ml-3 mr-3">
                    @if(Auth::user()->estAdmin())
                    <a href="{{ URL::to('sondages/'.$sondage->id.'/showAdminSondage') }}">
                        <button type="button" class="btn btn-primary btn-sm">Editer</button>
                        </a>
                        @endif
                </div>
            @endauth
        </div>  
        
        
    <!--canany(['update', 'delete'], $sondage)-->
            
        
        
    <!--endcanany-->
        
    @endforeach
    
    </div>
     {{ $sondages->links() }}
</div>

@endsection
