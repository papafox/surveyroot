@extends('layouts.app')

@section('content')


<div class="container">
    <div class="list-group">
        
    @foreach ($questions as $question)
    
        <div class="list-group-item">
            <div class="d-flex pt-3 pb-3">
                <h2> Liste des questions</h2>
                <div class="d-flex float-left ml-4">
                
                    <!--<a href="{ URL::to('/questions/$sondage->id/createQ') }}">-->
                        <a href="{{ URL::to('questions/'.$sondage->id.'/createQ') }}">
                    <button type="button" class="btn btn-primary btn-sm">Créer une autre question</button>
                </a>
                </div>
                
            </div>
            <hr>
            <h3>{{ $question->question_text}}</h3>
            
            <div class="d-flex float-left mr-3 pt-3">
                <a href="{{ URL::to('questions/'. $question->id) }}"> 
                    <button type="button" class="btn btn-primary btn-sm">Jouer</button>
                </a>
            </div>
            <div class="d-flex float-left ml-3 mr-3 pt-3">
                <a href="{{ URL::to('questions/'.$question->id.'/edit') }}">
                    <button type="button" class="btn btn-primary btn-sm">Modifier</button>
                 </a>
            </div>
            <div class="d-flex float-left pt-3">
                <form class="ml-3" action="/questions/{{ $question->id }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE"> 
                    <button type="submit" class="btn btn-primary btn-sm"> Supprimer </button>
                </form>
            </div>
            
        </div> 
            
    @endforeach 
    </div>
</div>
        
@endsection        
    

