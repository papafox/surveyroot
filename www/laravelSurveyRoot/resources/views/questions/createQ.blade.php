@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-12 d-flex justify-content-center pt-5">
            <h1>Test de personnalité </h1>
        </div>
        <div class="offset-2 col-sm-8">
            <!--<form action="/questions" method="post">-->
                <form action="/questions" method="post">
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Le formulaire contient des erreurs
                    </div>
                @endif

                @csrf
                <input type="hidden" name="sondage_id" value="{{$id_Sondage}}">
                <!--<input type="hidden" name="reponse_id" value="{$reponse->id}}">-->
                <div class="form-group">
                    <label for="question_text">Question</label>
                    <textarea type="text" class="form-control @error('question_text') is-invalid @enderror" 
                    id="question_text" name="question_text" placeholder="question"> {{ old('question_text') }} </textarea>
                    @error('question_text')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="reponse_text">Réponse n°1</label>
                    <textarea type="text" class="form-control @error('reponse_text') is-invalid @enderror" 
                    id="reponse_text1" name="reponse_text1" placeholder="reponse"> {{ old('reponse_text1') }} </textarea>
                    @error('reponse_text1')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="score_reponse">Score réponse n°1</label>
                    <input type="number" class="form-control @error('score_reponse') is-invalid @enderror" 
                    id="score_reponse1" name="score_reponse1" placeholder="score reponse" value="{{ old('score_reponse1') }}">
                    @error('score_reponse1')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="reponse_text">Réponse n°2</label>
                    <textarea type="text" class="form-control @error('reponse_text') is-invalid @enderror" 
                    id="reponse_text2" name="reponse_text2" placeholder="reponse"> {{ old('reponse_text2') }} </textarea>
                    @error('reponse_text2')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="score_reponse">Score réponse n°2</label>
                    <input type="number" class="form-control @error('score_reponse') is-invalid @enderror" 
                    id="score_reponse2" name="score_reponse2" placeholder="score reponse" value="{{ old('score_reponse2') }}">
                    @error('score_reponse2')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="reponse_text">Réponse n°3</label>
                    <textarea type="text" class="form-control @error('reponse_text') is-invalid @enderror" 
                    id="reponse_text3" name="reponse_text3" placeholder="reponse"> {{ old('reponse_text3') }} </textarea>
                    @error('reponse_text3')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="score_reponse">Score réponse n°3</label>
                    <input type="number" class="form-control @error('score_reponse') is-invalid @enderror" 
                    id="score_reponse3" name="score_reponse3" placeholder="score reponse" value="{{ old('score_reponse3') }}">
                    @error('score_reponse3')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
	            <button type="submit" class="btn btn-primary btn-sm">Créer une question</button>
            </form>
</div>
</div>

@endsection