@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-12 d-flex justify-content-center pt-5">
            <h1>Modification du test de personnalité </h1>
        </div>
        <div class="offset-2 col-sm-8">
            <form action="/questions/{{ $question->id }}/update" method="post">
                @method('PUT')
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Le formulaire contient des erreurs
                    </div>
                @endif

                @csrf
                <div class="form-group">
                    <label for="question_text">Question</label>
                    <textarea type="text" class="form-control @error('question_text') is-invalid @enderror" 
                    id="question_text" name="question_text" placeholder="question"> {{ old('question_text', $question->question_text) }} </textarea>
                    @error('question_text')
                        <span class="text-danger help-block">{{ $message }} </span>
                    @enderror
                </div>
                <?php $tmpint = 0 ?>
                @foreach ($question->reponses()->get() as $reponse)
                <?php $tmpint += 1 ?>
                    <div class="form-group">
                    <label for="reponse_text">Réponse n°{{$tmpint}}</label>
                        <textarea type="text" class="form-control @error('reponse_text{{$tmpint}}') is-invalid @enderror" 
                        id="reponse_text{{$tmpint}}" name="reponse_text{{$tmpint}}" placeholder="reponse"> {{ old('reponse_text'.$tmpint, $reponse->texte_reponse) }}
                        </textarea>
                        @error('reponse_text{{$tmpint}}')
                            <span class="text-danger help-block">{{ $message }} </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="score_reponse">Score réponse n°{{$tmpint}}</label>
                        <input type="number" class="form-control @error('score_reponse') is-invalid @enderror" 
                        id="score_reponse{{$tmpint}}" name="score_reponse{{$tmpint}}" placeholder="score reponse" value="{{ old('score_reponse'.$tmpint, $reponse->score_reponse) }}">
                        @error('score_reponse{{$tmpint}}')
                            <span class="text-danger help-block">{{ $message }} </span>
                        @enderror
                    </div>
                @endforeach
	            <button type="submit" class="btn btn-primary btn-sm">Modifier la question</button>
            </form>
</div>
</div>

@endsection