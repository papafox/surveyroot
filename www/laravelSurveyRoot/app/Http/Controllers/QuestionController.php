<?php

namespace App\Http\Controllers;

use App\Profil;
use Illuminate\Http\Request;

use App\Sondage;
use App\Question;
use App\User_sondage;
use App\Reponse;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Recuperer toutes les questions et les stocker dans un variable
        $questions = Question::all();

        return view('questions.indexQ')->with('questions',$questions);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //public function create()
    //{
        //Récupérer toutes les questions et les stocker dans une variable
        //$questions = Question::all();
        //$reponses = Reponse::all();
        

        //Envoyer les données vers la vue createQ.blade php"
        //return view('questions.createQ');
    //}

    public function createQ($id_Sondage){
       return view('questions.createQ')->with('id_Sondage',$id_Sondage);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$reponse_id= $request->input('reponse_id');
        $sondage_id = $request->input('sondage_id');
        //Valider les données
        $request->validate([
            'question_text'=> 'required|max:255',
            'reponse_text1'=> 'required|max:955',
            'score_reponse1'=> 'required|integer',
            'reponse_text2'=> 'required|max:955',
            'score_reponse2'=> 'required|integer',
            'reponse_text3'=> 'required|max:955',
            'score_reponse3'=> 'required|integer'
        ]);
    
    $reponse1 = new Reponse();
    $reponse2 = new Reponse();
    $reponse3 = new Reponse();
    $question = new Question();

        $question->question_text = $request->input("question_text");
        $question->timestamps = false;
        $question->sondage_id = $sondage_id;
        $question->save();
        $reponse1->question_id = $question->id;
        $reponse1->texte_reponse = $request->input('reponse_text1');
        $reponse1->score_reponse = $request->input('score_reponse1');
        $reponse1->timestamps = false;
        $reponse2->question_id = $question->id;
        $reponse2->texte_reponse = $request->input('reponse_text2');
        $reponse2->score_reponse = $request->input('score_reponse2');
        $reponse2->timestamps = false;
        $reponse3->question_id = $question->id;
        $reponse3->texte_reponse = $request->input('reponse_text3');
        $reponse3->score_reponse = $request->input('score_reponse3');
        $reponse3->timestamps = false;
   
    
    $reponse1->save();
    $reponse2->save();
    $reponse3->save();
    

    //Rediriger vers la page liste des sondages
    //return redirect::to('/questions');
    //return redirect::to('/sondages');
    //$sondages = Sondage::all();

    //return view('sondages.index')->with('sondages', $sondages);
    return redirect('/sondages/'.$sondage_id.'/showAdminSondage');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //retrouver la question en fonction de l'id
        $question = Question::find($id);

        return view('sondages.jouer')->with('question', $question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editQ($id)
    {
        //retrouver la question en fonction de l'id
        $question = Question::find($id);
        

        //return view('questions.editQ')->with('question', $question);
        return view('questions.editQ')->with('question',$question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_question)
    {

        //Valider les données
        $request->validate([
            'question_text'=> 'required|max:255',
            'reponse_text1'=> 'required|max:955',
            'score_reponse1'=> 'required|integer',
            'reponse_text2'=> 'required|max:955',
            'score_reponse2'=> 'required|integer',
            'reponse_text3'=> 'required|max:955',
            'score_reponse3'=> 'required|integer'
        ]);
        //Récupérer les questions/réponses en base de données
        $question = Question::find($id_question);

        $question->question_text = $request->input("question_text");
        $question->timestamps = false;
        $question->save();


        $reponses = $question->reponses()->get();

        $tmpint = 0;
        foreach ($reponses as $reponse){
            $tmpint += 1;
            $reponse->texte_reponse = $request->input('reponse_text'.$tmpint);
            $reponse->score_reponse = $request->input('score_reponse'.$tmpint);
            $reponse->timestamps = false;
            $reponse->save();
        }



        //Rediriger vers la page questions
        return redirect('/sondages/'.$question->sondage_id.'/showAdminSondage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //detruire tout les reponses a la question avant de detruire la question
        $question = Question::find($id);
        $sondage_id = $question->sondage_id;
        foreach ($question->reponses()->get() as $reponse){
            Reponse::destroy($reponse->id);
        }
        //Retrouver et supprimer le profil
        Question::destroy($id);
        //Rediriger 
        return redirect('/sondages/'.$sondage_id.'/showAdminSondage');
    }
}
