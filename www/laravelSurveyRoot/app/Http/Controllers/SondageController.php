<?php

namespace App\Http\Controllers;

use App\Profil;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use App\Sondage;
use App\Question;
use App\User_sondage;

class SondageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //Recuperer tous les sondages et les stocker dans un variable
        $sondages = Sondage::orderBy("created_at","desc")->simplePaginate(5);

        return view('sondages.index')->with('sondages', $sondages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Envoyer la vue create
        return view('sondages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Valider les données
        $request->validate([
            'titre'=> 'required|max:200'
        ]);

    $sondage = new Sondage($request->all());


    $sondage->save();


    //Rediriger vers la page liste des sondages
    return redirect('/sondages/'.$sondage->id.'/showAdminSondage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //retrouver le sondage en fonction de l'id
        $sondage = Sondage::find($id);

        // Injecter le sondage dans la page jouer
        // generer la page jouer.blade.php et la retourner
        return view('sondages.jouer')->with('sondage', $sondage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Récupérer la recette en base de données
        $sondage = Sondage::find($id);
        //Injecter la recette dans la page edit
        //Générer la page edit.blade.php et la retourner
        return view('sondages.edit')->with('sondage', $sondage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Valider les données
        $request->validate([
            'titre'=>'required|max:200'
        ]);
        //Récupérer les sondages en base de données
        $sondage = Sondage::find($id);
        //Modifier les attributs du sondage avec les données du formulaire
        $sondage->titre = $request->input("titre");
        //Sauvegarder les changements en base de données
        $sondage->save();
        //Rediriger vers la page du sondage
        return redirect('/sondages/'.$sondage->id.'/showAdminSondage');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //Retrouver et supprimer la recette de la base de données
         Sondage::destroy($id);
         //Rediriger vers la page d'accueil
         return redirect('/sondages');
     
    }
    public function resultat(Request $request)
    {     
        $resultat = 0;     
        $user_id = $request->input('user_id');
        $sondage_id = $request->input('sondage_id');



        // additionner tout les valeures du sondage
        foreach ($request->all() as $key => $valeur)
        {
            if ($key != "_token"&&$key != "user_id"&&$key != "sondage_id")
            {
                $resultat += $valeur;   
            }
        }
        // envoyer les scores sur la BD si il n'existe pas déjà
        if (!$this->userDejaJouer($user_id,$sondage_id)){
            $score = new User_sondage();
            $score->user_id = $user_id;
            $score->sondage_id = $sondage_id;
            $score->score = $resultat;
            $score->timestamps = false;

            $score->save();    
        }// else 

        //recuperer le profil selon de score
        $profil = Profil::where('sondage_id','=',$sondage_id)->where('score_min','<=',$resultat)->where('score_max','>=',$resultat)->first();
        //si aucun profil n'as été trouvé
        if (is_null($profil)){
            $profil = new Profil();
            $profil->description_profil = "Aucune profil trouvé";
        }

        //calculer le score max du sondage
        $scoreMax = $this->findScoreMaxSondage($sondage_id);

        //crée un tableau des variable a transferer sur la vue
        $data = [
            'score' => $resultat,
            'scoreMax' => $scoreMax,
            'profil' => $profil,
        ];

        //envoyer la vue avec le tableau de variable
        return view('sondages.resultat')->with('data',$data);      
    }
    public function showResultat($user_id,$sondage_id){

        $resultat = User_sondage::Where('sondage_id','=',$sondage_id)->Where('user_id','=',$user_id)->first()->score;


        //recuperer le profil selon de score
        $profil = Profil::where('sondage_id','=',$sondage_id)->where('score_min','<=',$resultat)->where('score_max','>=',$resultat)->first();
        //si aucune profil n'as été trouvé
        if (is_null($profil)){
            $profil = new Profil();
            $profil->description_profil = "Aucune profil trouvé";
        }
        //calculer le score max du sondage
        $scoreMax = $this->findScoreMaxSondage($sondage_id);
        //crée un tableau des variable a transferer sur la vue
        $data = [
            'score' => $resultat,
            'scoreMax' => $scoreMax,
            'profil' => $profil,
        ];

        //envoyer la vue avec le tableau de variable
        return view('sondages.resultat')->with('data',$data); 
    }
    public function findScoreMaxSondage($sondage_id){
        $questions = Question::Where('sondage_id','=',$sondage_id)->get();
        $scoreMax = 0;

        foreach ($questions as $question){
            $tmpScoreMax = 0;
            foreach($question->reponses()->get() as $reponse){
                if ($reponse->score_reponse > $tmpScoreMax){
                    $tmpScoreMax = $reponse->score_reponse;
                }
            }
            $scoreMax += $tmpScoreMax;
        }
        return $scoreMax;

    }
    public function findScoreMinSondage($sondage_id){
        $questions = Question::Where('sondage_id','=',$sondage_id)->get();
        $scoreMin = 0;

        foreach ($questions as $question){
            $scoreMin += $question->reponses()->min('score_reponse');
        }
        return $scoreMin;

    }

    public function userDejaJouer($user_id,$sondage_id){
        if (User_sondage::Where('user_id', '=', $user_id)->where('sondage_id','=',$sondage_id)->first()){
            return true;
        }else{
            return false;
        }
    }
    public function createProfil(){
        return view('sondages.createProfil');
    }
    public function storeProfil(Request $request, $sondage_id){

        // Valider les données
        $request->validate([
            'description' => 'required|max:255',
            'ScoreMin' => 'required|integer',
            'ScoreMax' => 'required|integer',
        ]);

        $profil = new Profil();

        $profil->description_profil = $request->input("description");
        $profil->score_min = $request->input('ScoreMin');
        $profil->score_max = $request->input('ScoreMax');
        $profil->sondage_id = $sondage_id;
        $profil->timestamps = false;

        $profil->save();

        return redirect('/sondages/'.$sondage_id.'/showAdminSondage');
    }
    public function destroyProfil($id)
    {
        $sondage_id = Profil::find($id)->first()->sondage_id;
         //Retrouver et supprimer le profil
         Profil::destroy($id);
         //Rediriger vers la page d'accueil
         return Redirect::back();
     
    }
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProfil($id)
    {
        //Récupérer le profil en base de données
        $profil = Profil::find($id);
        //Injecter le profil dans la page edit
        //Générer la page editProfil.blade.php et la retourner
        return view('sondages.editProfil')->with('profil', $profil);
    }

    public function updateProfil(Request $request, $id)
    {
        // Valider les données
        $request->validate([
            'description' => 'required|max:255',
            'ScoreMin' => 'required|integer',
            'ScoreMax' => 'required|integer',
        ]);

        //Récupérer les sondages en base de données
        $profil = Profil::find($id);
        //Modifier les attributs du sondage avec les données du formulaire
        $profil->description_profil = $request->input("description");
        $profil->score_min = $request->input('ScoreMin');
        $profil->score_max = $request->input('ScoreMax');
        $profil->timestamps = false;
        //Sauvegarder les changements en base de données
        $profil->save();
        //Rediriger vers la page du sondage
        return redirect('/sondages/'.$profil->sondage_id.'/showAdminSondage');

    }


    public function showAdminSondage($id)
    {
        //retrouver le sondage en fonction de l'id
        $sondage = Sondage::find($id);

        
        // definir les plages manquantes des profils
        $scoreMin = $this->findScoreMinSondage($id);
        $scoreMax = $this->findScoreMaxSondage($id);
        $nbrNotInPlage = [];
        $plages = [];
        for ($i=$scoreMin; $i <= $scoreMax; $i++) {
            $iInPlage = false;
            foreach ($sondage->profils()->get() as $profil) {
                if(($i >= $profil->score_min)&&($i <= $profil->score_max)){
                    $iInPlage = true;
                    $i = $profil->score_max ;
                    break;
                }
            }
            if (!$iInPlage){
                //$i n'est pas dans une plage d'un profil
                $nbrNotInPlage[] = $i;
            }
        }
        // mettre les nombre qui sont pas dans un profil sous forme de "plage"
        /*$tmpfirst = $nbrNotInPlage[0];
        $tmpLast = 0;
        for ($i=1; $i < count($nbrNotInPlage); $i++) {

            if (!$nbrNotInPlage[$i] === $tmpfirst-1){
                $tmpLast = $nbrNotInPlage[$i-1];
                $plages[] = $tmpfirst;
                $plages[] = $tmpLast;
            }
        }*/


        $data = [
            'sondage' => $sondage,
            'scoreMin' => $scoreMin,
            'scoreMax' => $scoreMax,
            'plages' => $nbrNotInPlage,

        ];


        // Injecter le sondage dans la page jouer
        // generer la page jouer.blade.php et la retourner
        return view('sondages.showSondageAdmin')->with('data', $data);
    }

}
