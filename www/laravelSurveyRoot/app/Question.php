<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $fillable = [
        'question_text',
        'sondage_id',
    ];

    public function sondage(){
        return $this->belongsTo('App\Sondage');
    }
    public function reponses(){
        return $this->hasMany('App\Reponse');
    }
}
