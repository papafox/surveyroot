<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    //
    protected $fillable = [
        'score_min',
        'score_max',
        'description_profil',
        'sondage_id',
    ];

    public function sondage(){
        return $this->belongsTo('App\Sondage');
    }
}
