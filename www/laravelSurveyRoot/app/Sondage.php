<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sondage extends Model
{
    //
    protected $fillable = [
    'titre',  
    ];
    public function users(){
    return $this->belongsToMany('App\User');
    }
    public function questions(){
    return $this->hasMany(Question::class);
    }
    public function profils(){
    return $this->hasMany(Profil::class);
    }
}
