<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sondages(){
        return $this->belongsToMany('App\Sondage');
    }

    public function estAdmin(){
        if ($this->admin === 1){
            return true;
        }else{
            return false;
        }
    }

    public function aDejaJouer($sondage_id){
        if (User_sondage::Where('user_id', '=', $this->id)->where('sondage_id','=',$sondage_id)->first()){
            return true;
        }else{
            return false;
        }
    }

}
