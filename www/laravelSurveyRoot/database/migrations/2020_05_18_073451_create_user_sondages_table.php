<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSondagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sondages', function (Blueprint $table) {
	  
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('sondage_id');
       	    $table->integer('score'); 
	    $table->primary(['user_id','sondage_id']);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('sondage_id')->references('id')->on('sondages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sondages');
    }
}
