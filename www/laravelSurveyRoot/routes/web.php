<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('sondages', 'SondageController');


//resultat
Route::post('/resultat', 'SondageController@resultat');
Route::get('/{user}/{sondage}/resultat','SondageController@showResultat');

//Creation des questions
Route::get('/questions/{sondage}/createQ','QuestionController@createQ');

Route::resource('questions', 'QuestionController');

//Modification des questions
Route::get('/questions/{question}/editQ','QuestionController@editQ');

Route::put('/questions/{question}/update','QuestionController@update');

// creation du profil
Route::get('/sondages/{sondage}/createProfil','SondageController@createProfil');

Route::post('/sondages/{sondage}/storeProfil','SondageController@storeProfil');

// supprimer la le profil
Route::get('/sondages/{profil}/DeleteProfil','SondageController@destroyProfil');

//modification du profil
Route::get('/sondages/{profil}/editProfil','SondageController@editProfil');

Route::put('/sondages/{profil}/updateProfil','SondageController@updateProfil');


//affichage de la page d'administration d'un sondage
Route::get('/sondages/{sondage}/showAdminSondage','SondageController@showAdminSondage');



